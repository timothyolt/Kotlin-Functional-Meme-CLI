import org.json.JSONObject
import java.awt.Desktop
import java.io.Console
import java.net.URI
import java.net.URL

fun main(args: Array<String>) {
    val memeGenerator = MemeGenerator()
    while(true) {
        TheExtremelyAccurateTimVerificationSystem(memeGenerator).verifyTim()
    }
}

class TheExtremelyAccurateTimVerificationSystem(private val memeGenerator: MemeGenerator) {

    fun verifyTim() {
        askQuestion()
        val answer = readLine() ?: ""
        val redirectUrl = when (determineYesNo(answer)) {
            IsTim.Tim -> {
                println("welcome, tim")
                "https://www.youtube.com/watch?v=SGl9dBOJp9E&ab_channel=ShuvoSarker"
            }
            IsTim.NotTim -> memeGenerator.uniqueMemeFor(answer, "WackyTicTacs")
            IsTim.Unknown -> memeGenerator.uniqueMemeFor(answer, "dankmemes")
        }
        redirect(redirectUrl)
    }

    private fun askQuestion() {
        println("Are you Tim?")
    }

    private enum class IsTim {
        Tim, NotTim, Unknown
    }

    private fun determineYesNo(answer: String): IsTim {
        return when {
            answer.lowercase() == "ultimatebuns" -> IsTim.Tim
            answer.lowercase().startsWith('n') -> IsTim.NotTim
            else -> IsTim.Unknown
        }
    }

    private fun redirect(url: String) {
        Desktop.getDesktop().browse(URI(url))
    }
}

class MemeGenerator {

    private val cache = mutableMapOf<String, String>()

    fun uniqueMemeFor(string: String, defaultSubreddit: String = ""): String {
        return cache.getOrPut(string.trimEnd().trimStart().lowercase()) {
            findRandomMeme(defaultSubreddit)
        }
    }

    private fun findRandomMeme(subreddit: String = ""): String {
        val memeJson = URL("https://meme-api.herokuapp.com/gimme/$subreddit")
            .openConnection()
            .getInputStream()
            .use { JSONObject(String(it.readBytes())) }
        return memeJson.getString("url")
    }
}
